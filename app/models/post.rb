class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence_of :title, :message=>"El campo esta vacio"
    validates_length_of :body, :in=> 10..400,:message=>"longitud no valida"
    belongs_to :user
end
