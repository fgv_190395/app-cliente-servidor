class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_of :body, :in => 20..400, :message =>"Longitud no válida"
end
