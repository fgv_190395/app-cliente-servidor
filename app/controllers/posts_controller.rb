class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
  
    @posts = Post.all
  end
  
  
  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    if user_signed_in?
      if cannot? :edit, @post
    flash[:alert]= "No tiene permisos para editar"
    redirect_to :action =>'index',:format=>'html'
    end 
    end
  
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id

    respond_to do |format|
      if @post.save
        flash[:success]="Se guardo correctamente"
        format.html{redirect_to @post}
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
         format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
        flash[:alert]="Su post no se guardo"
        
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        #format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        #format.json { render :show, status: :ok, location: @post }
       flash[:success]="Se actualizo correctamente"
        format.html{redirect_to @post}
      else

        #format.html { render :edit }
        #format.json { render json: @post.errors, status: :unprocessable_entity }
        flash[:alert]="Su post no se actualizo correctamente"
        format.html{render :edit}
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      #format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
      flash[:alert]="Se ha eliminado"
        format.html{redirect_to posts_url}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :body)
    end
end
